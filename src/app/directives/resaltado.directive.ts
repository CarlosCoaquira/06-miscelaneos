import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  @Input('appResaltado') color: string;

  constructor( private ele: ElementRef) {
    console.log('directiva llamada');

   }

   @HostListener('mouseenter') mouseEntro() {
    // this.ele.nativeElement.style.backgroundColor = 'yellow';
    this.resaltar( this.color || 'yellow' );
   }

   @HostListener('mouseleave') mouseSalio() {
    // this.ele.nativeElement.style.backgroundColor = null;
    this.resaltar( null );
   }

   private resaltar( color: string) {
    this.ele.nativeElement.style.backgroundColor = color;
   }

}
