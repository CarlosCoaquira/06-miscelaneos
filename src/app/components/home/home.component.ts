import { Component, OnInit, OnChanges, DoCheck, AfterContentInit,
        AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <app-ng-style></app-ng-style>
    <app-css></app-css>

    <app-clases></app-clases>

    <p [appResaltado]="'lightblue'">Hola de nuevo xD</p>

    <app-ng-switch></app-ng-switch>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit, OnChanges, DoCheck, AfterContentInit,
                          AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

  constructor() {
    console.log('ejecutando constructor');
   }

  ngOnInit(): void {
    console.log('ejecutando ngOnInit');
  }

  ngOnChanges(): void {
    console.log('ejecutando ngOnChanges');
  }

  ngDoCheck(): void {
    console.log('ejecutando ngDoCheck');
  }

  ngAfterContentInit(): void {
    console.log('ejecutando ngAfterContentInit');
  }

  ngAfterContentChecked(): void {
    console.log('ejecutando ngAfterContentChecked');
  }

  ngAfterViewInit(): void {
    console.log('ejecutando ngAfterViewInit');
  }

  ngAfterViewChecked(): void {
    console.log('ejecutando ngAfterViewChecked');
  }

  ngOnDestroy(): void {
    console.log('ejecutando ngOnDestroy');
  }


}
